import React, { useState, useReducer, useCallback, useEffect } from 'react'
import {
    ScrollView,
    View,
    Text,
    Button,
    StyleSheet,
    KeyboardAvoidingView,
    ActivityIndicator,
    Alert,
    CheckBox,
    Switch,
    Modal,
    Pressable,
    Platform
} from 'react-native'
import { useDispatch } from 'react-redux'
import { Entypo } from '@expo/vector-icons'

import Card from '../../components/UI/Card'
import Input from '../../components/UI/Input'
import { Colors } from '../../constants/App'
import * as authActions from '../../store/actions/auth'
import {HeaderButtons, Item} from "react-navigation-header-buttons";
import HeaderButton from "../../components/UI/HeaderButton";

const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'

const formReducer = (state, action) => {
    if (action.type === FORM_INPUT_UPDATE) {
        const updatedValues = {
            ...state.inputValues,
            [action.input]: action.value
        }

        const updatedValidities = {
            ...state.inputValidities,
            [action.input]: action.isValid
        }

        let updatedFormIsValid = true;
        for (const key in updatedValidities) {
            updatedFormIsValid = updatedFormIsValid && updatedValidities[key];
        }

        return {
            formIsValid: updatedFormIsValid,
            inputValidities: updatedValidities,
            inputValues: updatedValues
        }
    }
    return state
}

const AuthScreen = props => {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState()
    const [isSignUp, setIsSignUp] = useState(false)
    const [isService, setIsService] = useState(false)
    const [serviceInfo, setServiceInfo] = useState(false)
    const [consentPersonalData, setConsentPersonalData] = useState(false)
    const dispatch = useDispatch();

    const [formState, dispatchFormState] = useReducer(formReducer, {
        inputValues: {
            email: '',
            password: '',
            confirm_password: '',
        },
        inputValidities: {
            email: false,
            password: false,
            confirm_password: false,
        },
        formIsValid: false
    })

    const serviceSwitchHandler = () => setIsService(previousState => !previousState)

    const authHandler = async () => {
        let action;
        if (isSignUp) {
            action = authActions.signup(formState.inputValues.name, formState.inputValues.email, formState.inputValues.password, isService, consentPersonalData );
        } else {
            action = authActions.login(formState.inputValues.email, formState.inputValues.password);
        }

        setError(null);
        setIsLoading(true);

        if(isSignUp && !consentPersonalData){
            Alert.alert('Personal data permissions', 'Permission is required!', [{text: 'Okay'}]);
            setIsLoading(false);
        } else {
            try {
                await dispatch(action);
                if(isSignUp){
                    // props.navigation.navigate('profile')
                } else {
                    props.navigation.navigate('ProductsOverview')
                }

            } catch (err) {
                setError(err.message);
                setIsLoading(false);
            }
        }

    }

    useEffect(() => {
        if (error) {
            Alert.alert('Personal data permissions', error, [{text: 'Okay'}]);
        }
    }, [error])

    const inputChangeHandler = useCallback(
        (inputIdentifier, inputValue, inputValidity) => {
            dispatchFormState({
                type: FORM_INPUT_UPDATE,
                value: inputValue,
                isValid: inputValidity,
                input: inputIdentifier
            });
        },
        [dispatchFormState]
    );

    return (
        <KeyboardAvoidingView
            behavior="height"
            keyboardVerticalOffset={15}
            style={styles.screen}
        >
            <Card style={styles.authContainer}>
                <ScrollView>
                    {isSignUp && (<Input
                        id="name"
                        label="Name"
                        keyboardType="default"
                        required
                        autoCapitalize="none"
                        errorText="Please enter a name."
                        onInputChange={inputChangeHandler}
                        initialValue=""
                    />
                    )}
                    <Input
                        id="email"
                        label="E-Mail"
                        keyboardType="email-address"
                        required
                        email
                        autoCapitalize="none"
                        errorText="Please enter a valid email address."
                        onInputChange={inputChangeHandler}
                        initialValue=""
                    />
                    <Input
                        id="password"
                        label="Password"
                        keyboardType="default"
                        secureTextEntry
                        required
                        minLength={8}
                        autoCapitalize="none"
                        errorText="Please enter a valid password(min 8 characters)."
                        onInputChange={inputChangeHandler}
                        initialValue=""
                    />
                    {isSignUp && (
                        <Input
                            id="confirm_password"
                            label="Confirm password"
                            keyboardType="default"
                            secureTextEntry
                            required
                            minLength={8}
                            isEqual={formState.inputValues.password}
                            autoCapitalize="none"
                            errorText="Your password and confirmation password do not match."
                            onInputChange={inputChangeHandler}
                            initialValue=""
                        />
                    )}
                    {isSignUp && (
                        <View style={styles.formGroup}>
                            <CheckBox
                                value={consentPersonalData}
                                onValueChange={setConsentPersonalData}
                                checked={consentPersonalData}
                                required
                            />
                            <Text style={styles.label}>Permission Personal data</Text>
                        </View>
                    )}
                    {isSignUp && (
                        <View style={styles.formGroup}>
                            <Switch
                                trackColor={{ false: "#767577", true: "#81b0ff" }}
                                thumbColor={isService ? "#f5dd4b" : "#f4f3f4"}
                                ios_backgroundColor="#3e3e3e"
                                onValueChange={serviceSwitchHandler}
                                value={isService}
                            />
                            <Text style={styles.label}>I provide service</Text>

                            <Pressable onPress={() => setServiceInfo(true)} >
                                <Entypo name="info-with-circle" size={24} color={Colors.accent} style={styles.serviceInfo} />
                            </Pressable>


                            <Modal
                                animationType="slide"
                                transparent={true}
                                visible={serviceInfo}
                                onRequestClose={() => {
                                    Alert.alert("Modal has been closed.");
                                    setServiceInfo(!serviceInfo);
                                }}
                            >
                                <View style={styles.centeredView}>
                                    <View style={styles.modalView}>
                                        <Text style={styles.modalText} >
                                            Toto zaskrtni ak ponukas sluzbu(mas fitko, robis kurz, alebo si online trener)
                                        </Text>
                                        <Pressable
                                            style={[styles.button, styles.buttonClose]}
                                            onPress={() => setServiceInfo(!serviceInfo)}
                                        >
                                            <Text style={styles.textStyle}>Close</Text>
                                        </Pressable>
                                    </View>
                                </View>
                            </Modal>

                        </View>
                    )}

                    <View style={styles.buttonContainer}>
                        { isLoading ? (
                            <ActivityIndicator
                                size="small"
                                color={Colors.primary}
                            />
                        ) : (
                            <Button
                                title={isSignUp ? "Sign Up" : "Login"}
                                color={Colors.primary}
                                onPress={authHandler}
                            />
                        )}
                    </View>
                    <View style={styles.buttonContainer}>
                        <Button title={`Switch to ${isSignUp ? 'Log In' : 'Sing Up'}`}
                                color={Colors.accent}
                                onPress={() => {
                                    setIsSignUp(prevState => !prevState)
                                }}
                        />
                    </View>
                </ScrollView>
            </Card>
        </KeyboardAvoidingView>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'Authenticate',
        headerLeft:() => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        ),
    }
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        alignItems: 'center',
        paddingTop: 20
    },
    authContainer: {
        width: '80%',
        maxWidth: 400,
        maxHeight: 600,
        padding: 20,
    },
    gradient: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonContainer: {
        marginTop: 10
    },
    formGroup: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    serviceInfo: {
        marginLeft: 10,
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 4,
        elevation: 5
    },
    button: {
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    buttonClose: {
        backgroundColor: Colors.accent,
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
})

export default AuthScreen