import React, { useState } from 'react'
import { View, Text, FlatList, Button, StyleSheet, ActivityIndicator } from 'react-native'
import { useSelector, useDispatch } from 'react-redux'

import Card from '../../components/UI/Card'
import CartItem from '../../components/shop/CartItem'
import { Colors } from '../../constants/App'
import * as cartActions from '../../store/actions/cart'

const CartScreen = props => {
    const [isLoading, setIsLoading] = useState(false)
    const cartTotalAmount = useSelector(state => state.cart.totalAmount)
    const cartItems = useSelector(state=> {
        const transformedCartItems =[]
        for (const key in state.cart.items) {
            transformedCartItems.push({
                productId: key,
                productName: state.cart.items[key].productName,
                productPrice: state.cart.items[key].productPrice,
                productImgUrl: state.cart.items[key].productImgUrl,
                quantity: state.cart.items[key].quantity,
                sum: state.cart.items[key].sum,
            })
        }
        return transformedCartItems.sort( (a, b) =>
            a.productId > b.productId ? 1 : -1
        )
    })

    const dispatch = useDispatch()

    const sendOrderHandler = async () => {
        // setIsLoading(true)
        console.log('order placed')
        // TODO ADD ORDER
        // await dispatch(orderActions.addOrder(cartItems, cartTotalAmount))
        // setIsLoading(false)
    }

    return (
        <View style={styles.screen}>
            <Card style={styles.summary}>
                <Text style={styles.summaryText}>
                    Total:
                    <Text style={styles.summaryText}>
                        $ { Math.round(cartTotalAmount.toFixed(2) * 100) / 100}
                    </Text>
                </Text>
                {isLoading ? <ActivityIndicator size="small" color={Colors.primary} /> :
                    <Button
                        color={Colors.success}
                        title="Order Now"
                        disabled={cartItems.length === 0}
                        onPress={sendOrderHandler}
                    />
                }
            </Card>

            <FlatList
                data={cartItems}
                keyExtractor={item => item.productId}
                renderItem={itemData =>
                    <CartItem
                        quantity={itemData.item.quantity}
                        title={itemData.item.productName}
                        imgUrl={itemData.item.productImgUrl}
                        amount={itemData.item.sum}
                        deletable
                        onRemove={() => {
                            dispatch(cartActions.removeFromCart(itemData.item.productId))
                        }}
                    />
                }
            />
        </View>
    )
}

export const screenOptions = {
    headerTitle: 'Your Cart'
}

const styles = StyleSheet.create({
    screen: {
        margin: 20
    },
    summary: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        marginBottom: 20,
        padding: 10
    },
    summaryText: {
        fontSize: 18
    },
    summaryAmount: {
        color: Colors.primary
    }
})

export default CartScreen