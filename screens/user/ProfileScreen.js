import React, {useCallback, useState, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    KeyboardAvoidingView,
    Platform,
    Switch,
    Pressable,
    Modal, Alert
} from 'react-native'

import {HeaderButtons, Item} from 'react-navigation-header-buttons'
import LocationPicker from '../../components/shop/LocationPicker'
import HeaderButton from '../../components/UI/HeaderButton'
import ImagePicker from '../../components/UI/ImagePicker'
import Input from '../../components/UI/Input'
import Card from '../../components/UI/Card'

import * as formActions from '../../store/actions/form'

import { Colors } from '../../constants/App'
import {Entypo} from "@expo/vector-icons";

const ProfileScreen = props => {
    const location = useSelector(state => state.filter.location)
    const user = useSelector(state => state.auth.user)
    const [ error, setError ] = useState(false)
    const form = useSelector(state => state.form)
    const [selectedLocation, setSelectedLocation] = 'lat' in location ? useState(location) : useState()
    const [isService, setIsService] = useState(user.role_id === 3 ? true : false)
    const [serviceInfo, setServiceInfo] = useState(false)
    const dispatch = useDispatch()

    const inputValues = {
        name: user.name,
        email: user.email
    }

    const inputValidates = {
        name: false,
        email: false
    }

    const setFormReducer = useCallback(async () => {
        try {
            await dispatch(formActions.defaultFormInputs(inputValues, inputValidates))
        } catch(err) {
            setError(err.message)
        }
    }, [dispatch, formActions, setError])

    useEffect(() => {
        setFormReducer()
    }, [setFormReducer])

    useEffect(() => {
        props.navigation.setOptions({
            headerRight: () => (
                <HeaderButtons HeaderButtonComponent={HeaderButton}>
                    <Item
                        title='Cart'
                        iconName={Platform.OS === 'android' ? 'md-save-outline' : 'ios-save-outline'}
                        onPress={submitHandler}
                    />
                </HeaderButtons>
            )
        })
    }, [submitHandler])

    const serviceSwitchHandler = () => setIsService(previousState => !previousState)

    const inputChangeHandler = useCallback((inputIdentifier, inputValue, inputValidity) => {
        dispatch(formActions.formInputUpdate({
            input: inputIdentifier,
            value: inputValue,
            isValid: inputValidity,
        }))
    }, [dispatch, formActions])

    const locationPickedHandler = useCallback(location => {
        setSelectedLocation(location)
    }, [])

    const submitHandler = useCallback(async () => {
        console.log('tralala TODO submit handler')
        console.log(form.isValid)
    }, [])

    return (
        <KeyboardAvoidingView
            behavior="height"
            keyboardVerticalOffset={15}
            style={styles.screen}
        >
            <Card style={styles.profileContainer}>
                <ScrollView style={styles.scroll}>
                    <Input
                        id="name"
                        label="Name"
                        keyboardType="default"
                        autoCapitalize="none"
                        errorText="Please enter valid name."
                        onInputChange={inputChangeHandler}
                        initialValue={user.name}
                        required
                    />
                    <Input
                        id="email"
                        label="E-Mail"
                        keyboardType="email-address"
                        required
                        email
                        autoCapitalize="none"
                        errorText="Please enter a valid email address."
                        onInputChange={inputChangeHandler}
                        initialValue={user.email}
                    />
                    <View style={styles.formGroup}>
                        <Switch
                            trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={isService ? "#f5dd4b" : "#f4f3f4"}
                            ios_backgroundColor="#3e3e3e"
                            onValueChange={serviceSwitchHandler}
                            value={isService}
                        />
                        <Text style={styles.label}>I provide service</Text>

                        <Pressable onPress={() => setServiceInfo(true)} >
                            <Entypo name="info-with-circle" size={24} color={Colors.accent} style={styles.serviceInfo} />
                        </Pressable>

                        <Modal
                            animationType="slide"
                            transparent={true}
                            visible={serviceInfo}
                            onRequestClose={() => {
                                Alert.alert("Modal has been closed.");
                                setServiceInfo(!serviceInfo);
                            }}
                        >
                            <View style={styles.centeredView}>
                                <View style={styles.modalView}>
                                    <Text style={styles.modalText} >
                                        Toto zaskrtni ak ponukas sluzbu(mas fitko, robis kurz, alebo si online trener)
                                    </Text>
                                    <Pressable
                                        style={[styles.button, styles.buttonClose]}
                                        onPress={() => setServiceInfo(!serviceInfo)}
                                    >
                                        <Text style={styles.textStyle}>Close</Text>
                                    </Pressable>
                                </View>
                            </View>
                        </Modal>
                    </View>
                    {isService && (
                        <View>
                            <Input
                                id="company_name"
                                label="Company name"
                                keyboardType="default"
                                required={isService}
                                autoCapitalize="none"
                                errorText="Please enter a valid company name."
                                onInputChange={inputChangeHandler}
                                initialValue={user.companyName}
                            />
                            <ImagePicker/>
                            <LocationPicker
                                location={selectedLocation}
                                navigation={props.navigation}
                                onLocationPicked={locationPickedHandler}
                            />
                            <Input
                                id="short_desc"
                                label="Short decsription"
                                keyboardType="default"
                                autoCapitalize="none"
                                minLength={1}
                                maxLength={158}
                                errorText="Please, fill short description, min 5 max 158 characters."
                                onInputChange={inputChangeHandler}
                                initialValue={user.name}
                                required={isService}
                            />
                            <Input
                                id="desc"
                                label="Description"
                                keyboardType="default"
                                autoCapitalize="none"
                                errorText="Please, fill description of your company."
                                onInputChange={inputChangeHandler}
                                initialValue={user.email}
                                multiline={true}
                                required={isService}
                            />
                        </View>
                    )}
                </ScrollView>
            </Card>
        </KeyboardAvoidingView>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'My account',
        headerLeft:() => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        ),
    }
};

const styles = StyleSheet.create({
    screen:{
        flex: 1,
        alignItems: 'center',
        marginBottom: 20,
        paddingBottom: 20,
    },
    profileContainer: {
        width: '100%',
        maxWidth: 400,
        maxHeight: '100%',
    },
    formGroup: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    scroll: {
        width: '100%',
        padding: 10,
        paddingBottom: 30,
        marginBottom: 30,
    }
})

export default ProfileScreen