export const Colors = {
    primary: '#0f8077',
    accent: '#ffc107',
    success: 'green',
    warning: 'orange',
    danger: 'red',
    primaryTransparent: 'rgba(15,128,119, 0.8)',
}