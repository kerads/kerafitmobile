import React, {useState} from 'react'
import {Image, StyleSheet, Text, View, Alert, TouchableOpacity, Platform} from 'react-native'
import * as ImagePicker from 'expo-image-picker'
import * as Permissions from 'expo-permissions'
import { Ionicons, Entypo } from '@expo/vector-icons'

import ENV from '../../env'
import { Colors } from "../../constants/App";

const ImgPicker = props => {
    const [pickedImage, setPickedImage] = useState(`${ENV.storageUrl}images/gym/best belt.jpeg`)

    const verifyPermissions = async () => {
        const result = await Permissions.askAsync(Permissions.CAMERA)
        if (result.status !== 'granted') {
            Alert.alert('Insufficient Permission!', 'You need to granted camera permissions to use this app.', [{text: 'Okay'}])
            return false
        }
        return true
    }

    const openImagePickerAsync = async () => {
        const hasPermission = await verifyPermissions()
        if (!hasPermission){
            return
        }

        let pickerResult = await ImagePicker.launchImageLibraryAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.All,
                allowsEditing: true,
                aspect: [16, 9],
                quality: 0.5
            }
        )
        setPickedImage(pickerResult)
        console.log(pickerResult)
    }

    const openCamImagePicker = async () => {
        const hasPermission = await verifyPermissions()
        if (!hasPermission){
            return
        }

        const pickerResult = await ImagePicker.launchCameraAsync({
            allowsEditing: true,
            aspect: [16, 9],
            quality: 0.5
        });

        setPickedImage(pickerResult)
        console.log(pickerResult)
    }

    return (
        <View style={styles.border}>
            <Text style={styles.header}>Image or logo</Text>
            <Image source={{ uri: pickedImage.uri}} style={styles.image}/>
            <Text>Get photo from:</Text>

            <View style={styles.buttonsContent}>
                <TouchableOpacity onPress={openImagePickerAsync} style={styles.button}>
                    <Entypo
                        name="folder-images"
                        size={24} color="white"
                    />
                    <Text style={styles.buttonText}>Gallery</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={openCamImagePicker} style={styles.button}>
                    <Ionicons
                        style={styles.star}
                        name={Platform.OS === 'android' ? 'md-camera-outline' : 'ios-camera-outline'}
                        size={24}
                        color="white"
                    />
                    <Text style={styles.buttonText}>Camera</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    border:{
        flex: 1,
        marginTop: 10,
        marginBottom: 10,
        borderColor: 'grey',
        borderWidth: 1,
        borderRadius: 10,
        maxHeight: 320,
    },
    header:{
        width: '100%',
        fontSize: 20,
        fontWeight: 'bold',
    },
    image: {
        width: '100%',
        height: '100%',
        minWidth: 400,
        maxHeight: 200,
        backgroundColor: 'silver',

    },
    buttonsContent: {
        marginTop: 5,
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        textAlign: 'center',
    },
    button: {
        width: 70,
        maxHeight: 50,
        borderWidth: 1,
        borderRadius: 5,
        borderColor: 'grey',
        backgroundColor: Colors.primary,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        color: 'white'
    }
})

export default ImgPicker