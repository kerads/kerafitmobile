import React from 'react'
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
    Button
} from 'react-native'
import { Ionicons } from '@expo/vector-icons'

import Card from '../UI/Card'
import {Colors} from "../../constants/App";

const ProductItem = props => {
    let TouchableCmp = TouchableOpacity;
    if(Platform.OS === 'android' && Platform.version >= 21){
        TouchableCmp = TouchableNativeFeedback;
    }

    return(
        <Card style={styles.product}>
            <View style={styles.touchable}>
                <TouchableCmp onPress={props.onSelect} useForeground>
                    <View style={styles.imageContainer}>
                        <ImageBackground
                            style={styles.image}
                            source={{
                                uri: props.image,
                                cache: 'only-if-cached'
                            }}
                        >
                            <View style={styles.starContainer}>
                                <Ionicons
                                    type="Ionicons"
                                    style={styles.star}
                                    name={Platform.OS === 'android' ? 'md-star' : 'ios-star'}
                                    size={50}
                                    color="gold"
                                />
                                <Text style={styles.iconText}>5.5</Text>
                            </View>
                            <Text style={styles.validTo}>Valid to: {props.validTo}</Text>
                        </ImageBackground>
                    </View>
                    <View style={styles.details}>
                        <Text style={styles.title}>{props.name}</Text>
                        <Text style={styles.price}>${props.price}</Text>
                    </View>
                    <View style={styles.actions}>
                        <Button
                            color={Colors.primary}
                            title="View Detail"
                            onPress={props.onSelect}
                        />
                        <Button
                            color={Colors.primary}
                            title="To Cart"
                            onPress={props.onAddToCart}
                        />

                    </View>
                </TouchableCmp>
            </View>
        </Card>
    );
}

const styles = StyleSheet.create({
    product: {
        width: '90%',
        height: 300,
        margin: 20
    },
    touchable: {
        borderRadius: 10,
        overflow: 'hidden'
    },
    imageContainer: {
        width: '100%',
        height: '60%',
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
        overflow: 'hidden'
    },
    image: {
        flexGrow: 1,
        width: null,
        height: null,
        justifyContent: 'flex-end'
    },
    details: {
        alignItems: 'center',
        height: '17%',
        padding: 10
    },
    title: {
        // fontFamily: 'open-sans-bold',
        fontSize: 18,
        marginVertical: 2
    },
    price: {
        // fontFamily: 'open-sans',
        fontSize: 14,
        color: '#888'
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: '23%',
        paddingHorizontal: 20
    },
    starContainer: {
        position: 'absolute',
        alignSelf: 'center',
        top: -5,
    },
    star: {
        alignSelf: 'center'
    },
    iconText: {
        position: 'absolute',
        alignSelf: 'center',
        top: 17
    },
    validTo: {
        textAlign: 'center',
        alignSelf: 'flex-end',
        backgroundColor: 'rgba(255,255,255, 0.8)',
        padding:2,
        borderTopLeftRadius: 5
    }
})

export default ProductItem;