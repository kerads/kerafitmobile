import { SET_FILTER, GET_FILTER, DELETE_FILTER } from "../actions/filter";

const initialState = {
    location: [],
    distance: ''
}

export default (state = initialState, action) => {
    switch (action.type) {
        case SET_FILTER:
            return {
                ...state,
                location: action.location,
                distance: action.distance,
            }
        case GET_FILTER:
            return {
                ...state,
                location: this.location,
                distance: this.distance,
            }
        case DELETE_FILTER:
            return {
                ...state,
                location: [],
                distance: [],
            }
    }
    return state
}