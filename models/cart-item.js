class CartItem {
    constructor(quantity, productPrice, productName, productImgUrl, sum){
        this.quantity = quantity
        this.productPrice = productPrice
        this.productName = productName
        this.productImgUrl = productImgUrl
        this.sum = sum
    }
}

export default CartItem