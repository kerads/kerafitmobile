import React, { useState, useCallback } from 'react'
import { View, Text, Button, TextInput, StyleSheet, Platform } from 'react-native'
import {useDispatch, useSelector} from 'react-redux'

import * as filterActions from '../../store/actions/filter'
import LocationPicker from '../../components/shop/LocationPicker'
import { Colors } from '../../constants/App'

const FilterScreen = props => {
    const location = useSelector(state => state.filter.location)
    const distance = useSelector(state => state.filter.distance)
    const [selectedLocation, setSelectedLocation] = 'lat' in location ? useState(location) : useState()
    const [selectedDistance, setSelectedDistance] = distance > 0 ? useState(distance) : useState()
    const dispatch = useDispatch();

    const locationPickedHandler = useCallback(location => {
        setSelectedLocation(location)
    }, [])

    const saveFilterHandler = () => {
        dispatch(filterActions.setFilter(selectedLocation, selectedDistance));
        props.navigation.goBack();
    }

    const deleteFilterHandler = () => {
        dispatch(filterActions.deleteFilter());
        props.navigation.goBack();
    }

    const distanceInputHandler = (distance) => {
        setSelectedDistance(distance)
    }

    return(
        <View style={styles.filter}>
            <View style={styles.filterParams}>
                <View style={styles.locationPicker}>
                    <LocationPicker
                        location={selectedLocation}
                        navigation={props.navigation}
                        onLocationPicked={locationPickedHandler}
                    />

                </View>
                <View style={styles.distanceGroup}>
                    <Text style={styles.distanceText}>Distance (km)</Text>
                    <TextInput
                        style={styles.distanceInput}
                        keyboardType = 'numeric'
                        onChangeText={text => distanceInputHandler(text)}
                        value={selectedDistance}
                        maxLength={6}
                    />
                </View>

                <View style={styles.actionContainer}>
                    <Button title="Erase filter" color={Colors.warning} onPress={deleteFilterHandler} />
                    <Button title="Save filter" color={Colors.primary} onPress={saveFilterHandler} />
                </View>
            </View>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'Search Filter',
    }
};

const styles = StyleSheet.create({
    filter: {
        // flexDirection: 'row',
        padding: 3,
        paddingHorizontal: 15,
    },
    filterHeader: {
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 15,
    },
    filterText: {
        color: Platform.OS === 'android' ? 'white' : Colors.primary,
    },
    filterParams: {

    },
    locationPicker: {
        marginBottom: 15
    },
    mapPreview: {
        marginBottom: 10,
        width: '100%',
        height: 150,
        borderColor: '#ccc',
        borderWidth: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    actions: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: "100%"
    },
    actionContainer: {
        flexDirection: 'row',
        justifyContent: 'space-around',
        width: "100%",
        borderTopColor: 'black',
        marginBottom: 15
    },
    distanceGroup: {
        width: "100%",
        flexDirection: 'row',
        padding: 20,
    },
    distanceText: {
        width: "40%"
    },
    distanceInput: {
        width: "60%",
        backgroundColor: 'white',
        borderRadius: 5,
        borderWidth: 1,

    }

})

export default FilterScreen