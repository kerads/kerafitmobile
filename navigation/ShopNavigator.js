import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { Platform } from 'react-native'

import ProductOverviewScreen, { screenOptions as productsOverviewScreenOptions  } from '../screens/shop/ProductOverviewScreen'
import ProductDetailScreen, { screenOptions as productDetailScreenOptions  } from '../screens/shop/ProductDetailScreen'
import ProfileScreen, { screenOptions as profileScreenOptions } from '../screens/user/ProfileScreen'
import FilterScreen, { screenOptions as filterScreenOptions } from '../screens/shop/FilterScreen'
import CartScreen, { screenOptions as cartScreenOptions } from '../screens/shop/CartScreen'
import AuthScreen, { screenOptions as authScreenOptions } from '../screens/user/AuthScreen'
import MapScreen, { screenOptions as mapScreenOptions } from '../screens/shop/MapScreen'
import { Colors } from '../constants/App'

const defaultNavOptions = {
    headerStyle: {
        backgroundColor: Platform.OS === 'android' ? Colors.primary : ''
    },
    headerTintColor: Platform.OS === 'android' ? 'white' : Colors.primary
}

const ProductStackNavigator = createStackNavigator()

const ProductNavigator = () => {
    return (
        <ProductStackNavigator.Navigator screenOptions={defaultNavOptions}>
            <ProductStackNavigator.Screen
                className="Screen"
                name="ProductsOverview"
                component={ProductOverviewScreen}
                options={productsOverviewScreenOptions}
            />
            <ProductStackNavigator.Screen
                className="Screen"
                name="ProductDetail"
                component={ProductDetailScreen}
                options={productDetailScreenOptions}
            />
            <ProductStackNavigator.Screen
                className="Screen"
                name="Map"
                component={MapScreen}
                options={mapScreenOptions}
            />
            <ProductStackNavigator.Screen
                name="Cart"
                component={CartScreen}
                options={cartScreenOptions}
            />
            <ProductStackNavigator.Screen
                name="Filter"
                component={FilterScreen}
                options={filterScreenOptions}
            />
        </ProductStackNavigator.Navigator>
    )
}

const AuthStackNavigator = createStackNavigator()

export const AuthNavigator = () => {
    return (
        <AuthStackNavigator.Navigator screenOptions={defaultNavOptions}>
            <AuthStackNavigator.Screen
                name="Auth"
                component={AuthScreen}
                options={authScreenOptions}
            />
        </AuthStackNavigator.Navigator>
    )
}

const ProfileStackNavigator = createStackNavigator()

export const ProfileNavigator = () => {
    return (
        <ProfileStackNavigator.Navigator screenOptions={defaultNavOptions}>
            <ProfileStackNavigator.Screen
                name="Profile"
                component={ProfileScreen}
                options={profileScreenOptions}
            />
        </ProfileStackNavigator.Navigator>
    )
}

const ShopDrawerNavigator = createDrawerNavigator();

export const ShopNavigator = () => {
    return (
        <ShopDrawerNavigator.Navigator>
            <ShopDrawerNavigator.Screen
                name="Products"
                component={ProductNavigator}
                options={{ title: 'Products'}}
            />
            <ShopDrawerNavigator.Screen
                name="Auth"
                component={AuthNavigator}
                options={{ title: 'Auth'}}
            />
            <ShopDrawerNavigator.Screen
                name="Profile"
                component={ProfileNavigator}
                options={{ title: 'My profile'}}
            />
        </ShopDrawerNavigator.Navigator>
    )
}