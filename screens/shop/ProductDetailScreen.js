import React, { useState, useCallback, useEffect } from 'react'
import {View, Image, Text, StyleSheet, Platform} from 'react-native'
import {useDispatch, useSelector} from 'react-redux'

import * as productActions from "../../store/actions/products";
import ENV from "../../env";

import HorizontalCalendar from '../../components/shop/HorizontalCalendar'

const ProductDetailScreen = props => {
    const productId = props.route.params.productId
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState()
    const product = useSelector(state => state.products.selectedProduct );

    const dispatch = useDispatch()

    const loadProduct = useCallback( async () => {
        setError(null)
        try {
            await dispatch ( productActions.getProduct(productId))
        } catch (err) {
            setError(err.message)
        }
    }, [dispatch, setError])

    useEffect(() => {
        setIsLoading(true);
        loadProduct().then(() => {
            setIsLoading(false);
        });
    }, [dispatch, loadProduct]);

    return (
        <View style={styles.content}>
            <Image style={styles.productImage} source={{uri: (ENV.storageUrl + product.imgUrl)}}/>
            <Text style={styles.headingText}>{product.name}</Text>
            <Text style={styles.descriptionText}>{product.desc}</Text>
            <View style={styles.gallery}><Text>galeria TODO</Text></View>
            <HorizontalCalendar/>
            <View style={styles.calendar}><Text>Kalendar TODO</Text></View>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: navData.route.params.productName,
    }
};

const styles = StyleSheet.create({
    content: {
        flex: 1,
    },
    productImage:{
        width: '100%',
        height: 250,
        resizeMode: "cover",
    },
    headingText: {
        width: '100%',
        textAlign: "center",
        fontSize: 24,
    },
    descriptionText: {
        width: '100%',
        padding: 5,
    },
    gallery: {
        width: '100%',
    },
    calendar: {
        width: '100%'
    }
})

export default ProductDetailScreen