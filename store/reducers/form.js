import { DEFAULT_FORM_INPUTS, FORM_INPUT_UPDATE } from  '../actions/form'

const initialState = {
    inputValues: {},
    inputValidates: {},
    formIsValid: false
}

export default (state = initialState, action) => {
    console.log('action.inputValues')
    console.log(action.inputValues)
    switch (action.type){
        case DEFAULT_FORM_INPUTS:
            return {
                ...state,
                inputValues: action.inputValues,
                inputValidates: action.inputValidates,
                formIsValid: false
            }
        case FORM_INPUT_UPDATE:
            const updatedValues = {
                ...state.inputValues,
                [action.input]: action.value
            };
            const updatedValidates = {
                ...state.inputValidates,
                [action.input]: action.isValid
            };
            let updatedFormIsValid = true;
            for (const key in updatedValidates) {
                updatedFormIsValid = updatedFormIsValid && updatedValidates[key];
            }
            return {
                ...state,
                formIsValid: updatedFormIsValid,
                inputValidates: updatedValidates,
                inputValues: updatedValues
            };
    }
    return state;
}