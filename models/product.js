class Product {
    constructor(id, ownerId, name, imgUrl, desc, shortDesc, price, quantity, validTo, updatedAt) {
        this.id = id
        this.ownerId = ownerId
        this.name = name
        this.imgUrl = imgUrl
        this.desc = desc
        this.shortDesc = shortDesc
        this.price = price
        this.quantity = quantity
        this.validTo = validTo
        this.updatedAt = updatedAt
    }
}

export default Product;