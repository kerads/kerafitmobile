import { AUTHENTICATE, SET_DID_TRY_AL, LOGOUT } from '../actions/auth'

const initialState = {
    token: null,
    user: [],
    didTryAutoLogin: false
}

export default (state = initialState, action) => {
    switch (action.type) {
        case AUTHENTICATE:
            return {
                token: action.access_token,
                user: action.user,
                didTryAutoLogin: true,
            }
        case SET_DID_TRY_AL:
            return {
                ...state,
                didTryAutoLogin: true
            }
        case LOGOUT:
            return {
                ...initialState,
                didTryAutoLogin: true,
            }
        default:
            return state
    }
}