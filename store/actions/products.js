import Product from '../../models/product'

import ENV from '../../env'

export const GET_PRODUCTS = 'GET_PRODUCTS'
export const GET_PRODUCT = 'GET_PRODUCT'

export const fetchProducts = () => {
    return async ( dispatch ) => {
        try{
            const response = await fetch( `${ENV.apiUrl}product-list` )

            const resData = await response.json()
            const loadedProducts = []

            for(const key in resData) {
                loadedProducts.push(new Product(
                    parseInt(resData[key].id),
                    parseInt(resData[key].owner_id),
                    resData[key].name,
                    resData[key].img_url,
                    resData[key].desc,
                    resData[key].short_desc,
                    parseFloat(resData[key].price),
                    parseFloat(resData[key].quantity),
                    resData[key].valid_to,
                ))
            }

            dispatch({type: GET_PRODUCTS, products: loadedProducts});
        } catch (err) {
            throw err
        }
    }
}

export const getProduct = (id) => {
    return async ( dispatch ) => {
        try{
            const response = await fetch( `${ENV.apiUrl}get-product/${id}`)
            const resData = await response.json()
            const loadedProduct = new Product(
                resData.id,
                resData.owner_id,
                resData.name,
                resData.img_url,
                resData.desc,
                resData.short_desc,
                resData.price,
                resData.quantity,
                resData.valid_to,
            )

            dispatch({type: GET_PRODUCT, product: loadedProduct})
        } catch (err){
            throw err
        }

    }
}

