import React, { useState } from 'react'
import { View, StyleSheet } from 'react-native'
import { CalendarList } from 'react-native-calendars';

import { Colors } from '../../constants/App'

const HorizontalCalendar = props => {
    let date = new Date()
    let dd = date.getDate()
    let mm = date.getMonth() + 1
    let yyyy = date.getFullYear()
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    let today =  yyyy + "-" + mm + "-" + dd

    const [selectedDate, setSelectedDate] = useState(today)
    const [markedDates, setMarkedDates] = useState({});

    const setNewDaySelected = date => {
        const markedDate = Object.assign({});
        markedDate[date] = {
            selected: true,
            selectedColor: Colors.primary
        };
        setSelectedDate(date);
        setMarkedDates(markedDate);
    };


    return (
        <View style={styles.calendarContainer}>
            <CalendarList
                calendarWidth={320}
                current={selectedDate}
                minDate={today}
                pastScrollRange={24}
                futureScrollRange={24}
                markedDates={markedDates}
                horizontal
                pagingEnabled
                onDayPress={day => {
                    setNewDaySelected(day.dateString);
                }}
        />
        </View>
    )
}

const styles = StyleSheet.create({
    calendarContainer: {
        alignSelf: 'center',
        width: 320,
        margin: 15,
    }
})

export default HorizontalCalendar