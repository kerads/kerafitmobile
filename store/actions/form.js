export const FORM_INPUT_UPDATE = 'FORM_INPUT_UPDATE'
export const DEFAULT_FORM_INPUTS = 'DEFAULT_FORM_INPUTS'

export const formInputUpdate = (input) => {
    return { type: FORM_INPUT_UPDATE, input: input.input, value: input.value, isValid: input.isValid }
}

export const defaultFormInputs = (inputValues, inputValidates) => {
    return { type: DEFAULT_FORM_INPUTS, inputValues: inputValues, inputValidates: inputValidates }
}