import { GET_PRODUCTS, GET_PRODUCT } from '../actions/products'
import Product from '../../models/product'

const initialState = {
    availableProducts: [],
    selectedProduct: [],
}

export default (state = initialState, action) => {
    switch (action.type) {
        case GET_PRODUCTS:
            return {
                ...state,
                availableProducts: action.products,
                selectedProduct: state.selectedProduct
            }
        case GET_PRODUCT:
            return {
                ...state,
                availableProducts: state.availableProducts,
                selectedProduct: action.product
            }
    }
    return state
}