import { AsyncStorage } from 'react-native'

export const AUTHENTICATE = 'SIGNUP'
export const LOGOUT = 'LOGOUT'

export const SET_DID_TRY_AL = 'SET_DID_TRY_AL'
//
// export const setDidTryAL = () => {
//     return { type: SET_DID_TRY_AL }
// }

import ENV from '../../env'

export const authenticate = (user, accessToken) => {
    return dispatch => {
        dispatch({ type: AUTHENTICATE, user: user, accessToken: accessToken})
    }
}

export const signup = (name, email, password, isService, consentPersonalData ) => {
    return async dispatch => {
        const response = await fetch(
            `${ENV.apiUrl}registration`
            ,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    name: name,
                    email: email,
                    password: password,
                    role_id: isService ? 3 : 4,
                })
            }
        )

        const resData = await response.json();

        if(resData.code === 406) {
            let message = 'Something went wrong!'
            if (resData.errors.email) {
                message = resData.errors.email
            }

            throw new Error(message);
        }

        dispatch(authenticate(resData.user, resData.access_token))
        saveDataToStorage(resData.user, resData.access_token)
    }
}

export const login = (email, password) => {
    return async dispatch => {
        const response = await fetch(
            `${ENV.apiUrl}login`
            ,{
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password,
                })
            }
        )

        const resData = await response.json();

        if(resData.code === 406) {
            let message = 'Something went wrong!'

            throw new Error(message);
        }

        dispatch(authenticate(resData.user, resData.access_token))

        saveDataToStorage(resData.user, resData.access_token)
    }
}

export const logout = () => {
    AsyncStorage.removeItem('userData')
    return { type: LOGOUT }
}

const saveDataToStorage = (user, token) => {
    AsyncStorage.setItem('userData', JSON.stringify({
        user: user,
        token: token,
    }))
}