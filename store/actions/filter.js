export const SET_FILTER = 'SET_FILTER'
export const GET_FILTER = 'GET_FILTER'
export const DELETE_FILTER = 'DELETE_FILTER'

export const setFilter = (location, distance) => {
    return { type:SET_FILTER, location: location, distance: distance  }
}

export const getFilter = () => {
    return { type: GET_FILTER }
}

export const deleteFilter = () => {
    return { type: DELETE_FILTER }
}