import { ADD_TO_CART, REMOVE_FROM_CART } from "../actions/cart"
import CartItem from '../../models/cart-item'

const initialState = {
    items: {},
    totalAmount: 0
}

export default (state = initialState, action) => {
    switch (action.type) {
        case ADD_TO_CART:
            const addedProduct = action.product
            const prodPrice = addedProduct.price
            const prodName = addedProduct.name
            const prodImgUrl = addedProduct.imgUrl

            let updatedOrNewCart

            if(state.items[addedProduct.id]){
                updatedOrNewCart = new CartItem(
                    state.items[addedProduct.id].quantity + 1,
                    prodPrice,
                    prodName,
                    prodImgUrl,
                    state.items[addedProduct.id].sum + prodPrice
                )
            } else {
                updatedOrNewCart = new CartItem(1, prodPrice, prodName, prodImgUrl, prodPrice)
            }

            console.log('prodPrice')
            console.log(prodPrice)
            console.log('state.totalAmount')
            console.log(state.totalAmount)
            console.log('state.totalAmount + prodPrice')
            console.log(state.totalAmount + prodPrice)

            return {
                ...state,
                items: { ...state.items, [addedProduct.id]: updatedOrNewCart },
                totalAmount: state.totalAmount + prodPrice
            }

        case REMOVE_FROM_CART:
            const selectedCartItem = state.items[action.pid]
            const currentQty = selectedCartItem.quantity
            let updatedCartItems

            if(currentQty > 1) {
                const updatedCartItem = new CartItem(
                    selectedCartItem.quantity -1,
                    selectedCartItem.productPrice,
                    selectedCartItem.productName,
                    selectedCartItem.productImgUrl,
                    selectedCartItem.sum - selectedCartItem.productPrice
                )

                updatedCartItems = { ...state.items, [action.pid]: updatedCartItem }
            } else {
                updatedCartItems = { ...state.items }
                delete updatedCartItems[action.pid]
            }

            return {
                ...state,
                items: updatedCartItems,
                totalAmount: state.totalAmount - selectedCartItem.productPrice
            }
    }
    return state
}