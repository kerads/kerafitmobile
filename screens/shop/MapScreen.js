import React, { useState, useEffect, useCallback } from 'react'
import { StyleSheet, View, Text, Dimensions, TouchableOpacity, Platform } from 'react-native'
import MapView, { Marker } from 'react-native-maps'

import { Colors } from '../../constants/App'

const MapScreen = props => {
    const initialLocation = props.route.params?.initialLocation ?? {
        lat: 37.78,
        lng: -122.43,
    }

    const [selectedLocation, setSelectedLocation] = useState(initialLocation)
    const mapRegion = {
        latitude: selectedLocation.lat ? selectedLocation.lat : 37.78,
        longitude: selectedLocation.lng ? selectedLocation.lng : -122.43,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    }

    const selectLocationHandler = event => {
        setSelectedLocation({
            lat: event.nativeEvent.coordinate.latitude,
            lng: event.nativeEvent.coordinate.longitude
        });
    }

    const savePickedLocationHandler = useCallback( () => {
        if(!selectedLocation){
            return
        }
        props.navigation.navigate('ProductsOverview', {pickedLocation: selectedLocation})
    }, [])

    useEffect(() => {
        props.navigation.setOptions({
            headerRight: () => (
                <TouchableOpacity style={styles.headerButton} onPress={savePickedLocationHandler} >
                    <Text style={styles.headerButtonText}>Save</Text>
                </TouchableOpacity>
            )
        });
    }, [savePickedLocationHandler]);

    let markerCoordinates

    if(selectedLocation){
        markerCoordinates = {
            latitude: selectedLocation.lat,
            longitude: selectedLocation.lng,
        }
    }

    return (
        <View style={styles.container}>
            <MapView style={styles.map} region={mapRegion} onPress={selectLocationHandler}>
                {markerCoordinates && (
                    <Marker title="Pick Location" coordinate={markerCoordinates} />
                )}
            </MapView>
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'Map',
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
    map: {
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height,
    },
    headerButton: {
        marginHorizontal: 20
    },
    headerButtonText: {
        fontSize: 16,
        color: Platform.OS === 'android' ? 'white' : Colors.primary
    }
});

export default MapScreen;