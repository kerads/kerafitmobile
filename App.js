import { StatusBar } from 'expo-status-bar'
import React from 'react'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import ReduxThunk from 'redux-thunk'

import productsReducer from './store/reducers/products'
import filterReducer from './store/reducers/filter'
import formReducer from './store/reducers/form'
import authReducer from './store/reducers/auth'
import cartReducer from './store/reducers/cart'
import AppNavigator from './navigation/AppNavigator'

const rootReducer = combineReducers({
  cart: cartReducer,
  auth: authReducer,
  form: formReducer,
  filter: filterReducer,
  products: productsReducer,
})

const store = createStore(rootReducer, applyMiddleware(ReduxThunk));

export default function App() {
  return (
      <Provider store={store}>
        <AppNavigator/>
      </Provider>
  );
}