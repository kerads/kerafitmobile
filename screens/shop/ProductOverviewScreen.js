import React, { useState, useEffect, useCallback } from 'react'
import { FlatList, View, StyleSheet, Platform } from 'react-native'
import { useSelector, useDispatch } from "react-redux"
import { HeaderButtons, Item } from 'react-navigation-header-buttons'

import * as productActions from '../../store/actions/products'
import * as cartActions from '../../store/actions/cart'
import ProductItem from '../../components/shop/ProductItem'
import HeaderButton from '../../components/UI/HeaderButton'

import ENV from '../../env'

const ProductOverviewScreen = props => {
    const [isLoading, setIsLoading] = useState(false)
    const [error, setError] = useState()
    const location = useSelector(state => state.filter.location)
    const distance = useSelector(state => state.filter.distance)
    const products = useSelector(state => state.products.availableProducts );
    const dispatch = useDispatch()

    const loadProducts = useCallback( async () => {
        setError(null)
        try {
            await dispatch ( productActions.fetchProducts())
        } catch (err) {
            setError(err.message)
        }
    }, [dispatch, setError])

    useEffect(() => {
        setIsLoading(true);
        loadProducts().then(() => {
            setIsLoading(false);
        });
    }, [dispatch, loadProducts]);

    const selectItemHandler = (id, name) => {
        props.navigation.navigate('ProductDetail',{
            productId:id,
            productName: name
        })
    }

    return(
        <View>
            <FlatList
                style={styles.productList}
                data={products}
                keyExtractor={item => item.id.toString()}
                renderItem={itemData => (
                    <ProductItem
                        image={ ENV.storageUrl + itemData.item.imgUrl}
                        name={itemData.item.name}
                        price={itemData.item.price}
                        validTo={itemData.item.validTo}
                        onSelect={() => {
                            selectItemHandler(itemData.item.id, itemData.item.name);
                        }}
                        onAddToCart={() => {
                            dispatch(cartActions.addToCart(itemData.item))
                        }}
                    >
                    </ProductItem>
                )}
            />
        </View>
    )
}

export const screenOptions = navData => {
    return {
        headerTitle: 'All products',
        headerLeft:() => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Menu'
                    iconName={Platform.OS === 'android' ? 'md-menu' : 'ios-menu'}
                    onPress={() => {
                        navData.navigation.toggleDrawer()
                    }}
                />
            </HeaderButtons>
        ),
        headerRight: () => (
            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                <Item
                    title='Search'
                    iconName={Platform.OS === 'android' ? 'md-search' : 'ios-search'}
                    onPress={() => {
                        navData.navigation.navigate('Filter')
                    }}
                />
                <Item
                    title='Cart'
                    iconName={Platform.OS === 'android' ? 'md-heart' : 'ios-heart'}
                    onPress={() => {
                        navData.navigation.navigate('Cart')
                    }}
                />
            </HeaderButtons>
        )
    }
};

const styles = StyleSheet.create({
    screen: {
        flex:1
    },
    filterContainer: {
        top: 3,
        zIndex: 100,
        width: '100%',
        position: 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
    },
    productList: {
        paddingTop: 25
    },
})

export default ProductOverviewScreen